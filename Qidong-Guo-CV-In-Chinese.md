# 郭琪东

## 个人简介


[aside]

#### 联系方式
- guoqidong1990@gmail.com
- QQ: 292984062
- 手机: 15121032714
- Bitbucket: http://goo.gl/Zrc4ig

[/aside]

我是来自复旦大学计算机学院的研二学生，也是名 **程序员**。
<!-- I'm a student from Fudan University and also a **programmer**.  -->

我有丰富的 **项目经验**。
<!-- I have a lot of **Project Experience**. -->

我很欣赏前沿的酷炫实用的技术，目前对 **推荐系统** 感兴趣。
<!-- I'm fascinated with wonderful technology and currently interested in **Recommender System**. -->

我很享受书写优雅漂亮的代码，当然我也喜欢踢足球。
<!-- I enjoy writing beautiful code and yes, playing football. -->

## 教育经历 ##

### 复旦大学, 计算机科学与技术学院专业硕士学位, 2012 -2015

- **专业方向**: Web信息处理
- **主修课程**: 高级软件开发， 图论， 高级数据库调优， Web开发

### 东华大学, 计算机科学与技术学院学士学位, 2008 - 2012

- **主修课程**: 数据结构， 离散数学， 算法导论， 编译原理

## 项目经验

### 上海IBM CDL, 实习生, 2013.7 - 2013.12

**Web Capture Tool**

- Set up a http/https proxy to record network traffic using `Node.js`. 
	We use `MITM` technique to build a https proxy since https uses SSL protocal and all the message is encrypted.

- Create a browser extension to record user actions. 
	We use cross-browser extension framework -- `Kango` to build browser extension.

### 上海重点智能信息实验室, 学生, 2012 - 2015

**Baidu Promoting Management Tool**

-  Fetch Baidu statistics through `Web Service`. 
	Using the API provided by Baidu, we can download the data, such as click count, buy count, cost per keyword, etc.

-  Store processed data into local database for further analysis, such as giving a better advise for the price of each keyword.

**Browser Plugin Development**

- Learn to use cross platform browser plugin framework -- [Firebreath](http://www.firebreath.org/), which uses `CMake` to generate project.
- Use `gSoap` to set up a `Web Service` client, since the plugin sometimes needs the data from the server.
- Use `IE Automation` to launch WebBrowser, parse HTML and so on. 
	Muti-thread is used since we need to handle a lot of web pages at the same time.

**Recommender System** (Graduate thesis, not finished)

- To understand traditional methods of `recommender system`. 
	Among all the techniques, `Collaborative filtering` is widely used in practice.
	
- Focus on `context-based recommender system`. 
	Context information is something like time, location, etc.

## 获奖情况 ##

- 2009年东华大学ACM邀请赛铜牌
- 连续四年本科学习优秀奖
- 2009-2010学年上海计算机所奖学金
- 全国数学竞赛二等奖

## 主要技能 ##

- 熟悉设计模式, C/C++； 了解Java, C#, ruby, python。
- 英语水平：通过CET-4和CET-6，有流利的英文读写能力。
- 数学基础扎实，有一定的算法基础
