# Guo Qidong

## Profile


[aside]

#### Contacts
- guoqidong1990@gmail.com
- QQ: 292984062
- Mobile: 15121032714
- Bitbucket: http://goo.gl/Zrc4ig

[/aside]


I'm a student from Fudan University and also a **programmer**. 

I have a lot of **Project Experience**.

I'm fascinated with wonderful technology and currently interested in **Recommender System**.

I enjoy writing beautiful code and yes, playing football.

## Education ##

### Fudan University, Master of Science in School of Computer Science, 2012 -2015

- **Major Research Direction**: Network Informatics
- **Major Courses**: Advanced Software Development, Graph Theory, Database Tuning, Web Programming

### Donghua University, Bachelor of Science in School of Computer Science, 2008 - 2012

- **Major Courses**: Data Structure, Discrete Mathematics, Introduction to Algorithms, Compiler Theory

## Project Experience

### Shanghai IBM China Development Lab, Intern, 2013.7 - 2013.12

**Web Capture Tool**

- Set up a http/https proxy to record network traffic using `Node.js`. 
	We use `MITM` technique to build a https proxy since https uses SSL protocal and all the message is encrypted.

- Create a browser extension to record user actions. 
	We use cross-browser extension framework -- `Kango` to build browser extension.

### Shanghai Key Laboratory of Intelligent Information Processing, Student, 2012 - 2015

**Baidu Promoting Management Tool**

-  Fetch Baidu statistics through `Web Service`. 
	Using the API provided by Baidu, we can download the data, such as click count, buy count, cost per keyword, etc.

-  Store processed data into local database for further analysis, such as giving a better advise for the price of each keyword.

**Browser Plugin Development**

- Learn to use cross platform browser plugin framework -- [Firebreath](http://www.firebreath.org/), which uses `CMake` to generate project.
- Use `gSoap` to set up a `Web Service` client, since the plugin sometimes needs the data from the server.
- Use `IE Automation` to launch WebBrowser, parse HTML and so on. 
	Muti-thread is used since we need to handle a lot of web pages at the same time.

**Recommender System** (Graduate thesis, not finished)

- To understand traditional methods of `recommender system`. 
	Among all the techniques, `Collaborative filtering` is widely used in practice.
	
- Focus on `context-based recommender system`. 
	Context information is something like time, location, etc.

## Rewards Events ##

- In year 2009 Won ACM Tournament Bronze Medal
- In year 2009 Won Shanghai Computer Scholarship
- Won Learning Award of Excellence four years in a row

## Skills ##

- Familiar with `Design Pattern` and `C/C++`; Know about `Java` `C#` `Python` `Ruby` and `R`
- English skills: CET-4 and CET-6. Fluent English communication skill
- Solid foundation in mathematics and a good knowledge of algorithm
